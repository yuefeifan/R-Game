use bevy::prelude::*;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_startup_system(set_up)
        .add_system(show_player)
        .run();
}

#[derive(Default, Component)]
struct Player {
    name: String,
}

fn set_up(mut command: Commands) {
    command.spawn(Camera2dBundle::default());
    command.spawn(Player {
        name: "1".to_string(),
    });
    command.spawn(Player {
        name: "2".to_string(),
    });
    command.spawn(Player {
        name: "3".to_string(),
    });
    command.spawn(Player {
        name: "4".to_string(),
    });
}

fn show_player(query: Query<&Player>) {
    for player in query.iter() {
        println!("name {:?}", player.name);
    }
}
